package com.cmoon.batch7.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class MyStartedService extends Service {
    private static final String TAG =MyStartedService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "OnCreate, Thread name "+Thread.currentThread().getName());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "OnStartCommand, Thread name "+Thread.currentThread().getName());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "OnBind, Thread name "+Thread.currentThread().getName());
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "OnDestroy, Thread name "+Thread.currentThread().getName());
        super.onDestroy();
    }
}
