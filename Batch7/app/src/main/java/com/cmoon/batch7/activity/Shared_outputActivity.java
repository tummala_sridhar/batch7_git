package com.cmoon.batch7.activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.cmoon.batch7.R;

public class Shared_outputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_output);
        TextView txt_data = findViewById(R.id.txt_data);
        TextView txt_data_new = findViewById(R.id.txt_data_new);

        SharedPreferences preferences = getSharedPreferences("MYPREFER", MODE_PRIVATE);
        String Data = preferences.getString("key_name", null);
        String key_email = preferences.getString("key_email", null);
        String key_surname = preferences.getString("key_surname", null);
        int key_num = preferences.getInt("key_num", 0);

        txt_data.setText("Name: " + Data + "\nEmail: " + key_email + "\nNumber: " + key_num);


        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt("key_num", 2025);
        edit.clear();
        edit.apply();

        String Data2 = preferences.getString("key_name", null);
        String key_email2 = preferences.getString("key_email", null);
        int key_num2 = preferences.getInt("key_num", 0);

        txt_data_new.setText("Name: " + Data2 + "\nEmail: " + key_email2 + "\nNew Number: " + key_num2);

    }
}
