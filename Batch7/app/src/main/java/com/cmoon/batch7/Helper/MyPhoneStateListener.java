package com.cmoon.batch7.Helper;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.util.Log;


public class MyPhoneStateListener extends PhoneStateListener {

    private int signalStrengthValue = 0;

    @Override

    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
        if (signalStrength.isGsm()) {
            if (signalStrength.getGsmSignalStrength() != 99)
                signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
            else
                signalStrengthValue = signalStrength.getGsmSignalStrength();
        } else {
            signalStrengthValue = signalStrength.getCdmaDbm();
        }

        Log.e(getClass().getCanonicalName(), "Signal_Strength : " + signalStrengthValue);

    }
}
