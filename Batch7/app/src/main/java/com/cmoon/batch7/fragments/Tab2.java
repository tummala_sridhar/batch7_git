package com.cmoon.batch7.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmoon.batch7.R;


/**
 * Created by Sridhar on 02-06-2017.
 */

public class Tab2 extends Fragment {
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.tab2, container, false);

        TextView text1=(TextView)view.findViewById(R.id.textView);
        text1.setText("TAB1");


         return view;

    }


}
