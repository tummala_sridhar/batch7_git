package com.cmoon.batch7.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cmoon.batch7.R;

public class MainActivity extends AppCompatActivity {
    Button btn_submit, btn_submit12;

    boolean click = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_submit = findViewById(R.id.btn_submit);
        btn_submit12 = findViewById(R.id.btn_submit12);
        int count = 0;

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Welcome to android", Toast.LENGTH_SHORT).show();
                if (click) {
                    btn_submit12.setVisibility(View.VISIBLE);
                    click = false;
                } else {
                    btn_submit12.setVisibility(View.GONE);
                    click =true;
                }
            }
        });


        btn_submit12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Welcome to android", Toast.LENGTH_SHORT).show();

                Intent i1 = new Intent(MainActivity.this, SecondActivity.class);
                // startActivity(i1);
                btn_submit.setVisibility(View.GONE);

            }
        });
    }
}
