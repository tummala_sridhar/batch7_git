package com.cmoon.batch7.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.cmoon.batch7.R;
import com.cmoon.batch7.app.AppController;
import com.cmoon.batch7.app.CustomJsonObjectRequest;
import com.cmoon.batch7.models.Student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SubCategoryActivity extends AppCompatActivity {

    String categoryId = "";
    String TAG = "SubCategoryActivity";
    Button alert, alert2, alert3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            categoryId = b.getString("categoryId");
        }

        // categoryId = getIntent().getStringExtra("categoryId");
        //fetchData();

        alert = (Button) findViewById(R.id.alert1);
        alert2 = (Button) findViewById(R.id.alert2);
        alert3 = (Button) findViewById(R.id.alert3);


        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SubCategoryActivity.this);
                alertDialogBuilder.setIcon(R.drawable.a1);
                alertDialogBuilder.setTitle("Alert");
                alertDialogBuilder.setMessage("Hai welcome to android Tutorails");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke YES event
                        Toast.makeText(getApplicationContext(), "You clicked on Ok", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialogBuilder.show();
            }
        });


        alert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubCategoryActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you, sure you want delete this?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.a1);

                alertDialog.setCancelable(true);

                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        Toast.makeText(SubCategoryActivity.this, "clicked on cancel button", Toast.LENGTH_SHORT).show();
                    }
                });
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke YES event
                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();


            }
        });


        alert3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubCategoryActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Save File...");

                // Setting Dialog Message
                alertDialog.setMessage("Do you want to save this file?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.a1);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed YES button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on YES",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed No button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Netural "Cancel" Button
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed Cancel button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on Cancel",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Showing Alert Message
                alertDialog.show();

            }
        });

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(SubCategoryActivity.this);

                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("ProgressDialog"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(true);


                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(5000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }).start();


            }
        });


        alert2.setOnClickListener(new View.OnClickListener() {

            ProgressDialog progressDialog = new ProgressDialog(SubCategoryActivity.this);

            @Override
            public void onClick(View v) {

                progressDialog.setMax(100); // Progress Dialog Max Value
                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("ProgressDialog"); // Setting Title
                // progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); // Progress Dialog Style Horizontal
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Circle
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (progressDialog.getProgress() <= progressDialog.getMax()) {
                                // for circular
                            /*Thread.sleep(2000);
                            progressDialog.dismiss();*/

                                //For Horizontal

                                Thread.sleep(200);
                                handle.sendMessage(handle.obtainMessage());
                                if (progressDialog.getProgress() == progressDialog.getMax()) {
                                    progressDialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            Handler handle = new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    progressDialog.incrementProgressBy(2); // Incremented By Value 2
                }
            };

        });


        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "This is Toast example.", Toast.LENGTH_SHORT).show();
            }
        });

        alert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomAlert("This is custom toast");
            }
        });


    }

    private void fetchData() {

        String url = AppController.API_SERVER_URL + "get_sub_categories";
        HashMap<String, String> params = new HashMap<>();
        params.put("category_id", categoryId);

        Log.d(TAG, url);
        Log.d(TAG, params.toString());

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // TODO Auto-generated method stub
                        try {

                            Log.d(TAG, "RESPONSE : " + response.toString());
                            String result = response.getString("err_code");

                            if (result.equals("valid")) {
                                // studentsList.clear();

                                // JSONObject object = response.getJSONObject("data");
                                // JSONArray array = object.getJSONArray("category");

                               /* for (int i = 0; i < array.length(); i++) {
                                    Student model = new Student();
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    model.setID(jsonObject.getString("id"));
                                    model.setName(jsonObject.getString("name"));
                                    model.setAge(jsonObject.getString("num_count"));
                                    model.setImage(jsonObject.getString("image"));
                                    studentsList.add(model);
                                }*/
                            }

                            loadView(response.toString());
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        if (error instanceof TimeoutError) {
                            Toast.makeText(SubCategoryActivity.this, "Request Time out error. Your internet connection is too slow to work", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(SubCategoryActivity.this, "Connection Server error", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(SubCategoryActivity.this, "Network connection error! Check your internet Setting", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(SubCategoryActivity.this, "Parsing error", Toast.LENGTH_LONG).show();
                        }

                    }
                });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    private void loadView(String res) {
        TextView txt_response = findViewById(R.id.txt_response);
        txt_response.setText(res);
    }

    public void showCustomAlert(String msg) {

        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();
        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.custom_toast, null);
        Toast toast = new Toast(getApplicationContext());
        // Set layout to toast
        TextView txt_toast = toastRoot.findViewById(R.id.txt_toast);
        txt_toast.setText(msg);
        toast.setView(toastRoot);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

}
