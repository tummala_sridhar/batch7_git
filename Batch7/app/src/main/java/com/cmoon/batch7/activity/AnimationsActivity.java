package com.cmoon.batch7.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.cmoon.batch7.R;

public class AnimationsActivity extends AppCompatActivity implements Animation.AnimationListener {

    Animation animrotate;
    Animation animBounce;
    Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations);
        btn1 = findViewById(R.id.button5);

        // load animations
        animrotate = AnimationUtils.loadAnimation(this, R.anim.sequence_animation);
        animBounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        // set animation listeners
        animrotate.setAnimationListener(this);
        animBounce.setAnimationListener(this);
        // start fade in animation
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn1.startAnimation(animrotate);
            }
        });

    }


    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == animrotate) {
            Toast.makeText(getApplicationContext(), "Rotate Animation started", Toast.LENGTH_SHORT).show();
        } else if (animation == animBounce) {
            Toast.makeText(getApplicationContext(), "Bounce Animation started", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animrotate) {
            Toast.makeText(getApplicationContext(), "Rotate Animation END", Toast.LENGTH_SHORT).show();
            btn1.startAnimation(animBounce);
        } else if (animation == animBounce) {
            Toast.makeText(getApplicationContext(), "Bounce Animation END", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
