package com.cmoon.batch7.models;

public class Student {
    private String name;
    private String age;
    private String image;
    private String ID;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAge() {
        return age;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }
}
