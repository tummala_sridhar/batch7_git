package com.cmoon.batch7.activity.dataBase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cmoon.batch7.R;
import com.cmoon.batch7.app.AppController;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;


public class Onetime_loginActivity extends AppCompatActivity {

    EditText user, pass;
    Button btn_submit;
    private ProgressDialog pDialog;
    DBHelper dbh;
    String TAG = "Onetime_loginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbh = new DBHelper(this);

        if (dbh.row_count() == 1) {
            System.out.println("Entered into Student Login");
            Intent i1 = new Intent(Onetime_loginActivity.this, Login_successActivity.class);
            i1.putExtra("key", "danial_android");
            startActivity(i1);
        } else {
            setContentView(R.layout.activity_onetime_login);
            user = (EditText) findViewById(R.id.edt_email);
            pass = (EditText) findViewById(R.id.edt_pass);
            btn_submit = (Button) findViewById(R.id.btn_submit);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  LoginEx();

                    String user1 = user.getText().toString();
                    String password1 = pass.getText().toString();

                    dbh.deleteQuery_student_table();
                    dbh.insert_student_details(password1, user1);
                    Intent i1 = new Intent(Onetime_loginActivity.this, Login_successActivity.class);
                    i1.putExtra("key", "Aditya_android");
                    startActivity(i1);
                }
            });

        }

       // String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Toast.makeText(Onetime_loginActivity.this, token, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Token: " + token);
                    }
                });



    }


    private void LoginEx() {

// Tag used to cancel the request
        final String tag_json_obj = "json_obj_req";

        String user1 = user.getText().toString();
        String password1 = pass.getText().toString();


        System.out.println(user1 + "  " + password1);


        //  String url = "http://10.0.2.2/passes-api/login-api.php?mail="+editext+"&pwd="+edittext2;
        // username=tony&password=12345
        //  String url ="http://basolutions.in/hallpass/apis/student-log-api.php?username="+user1+"&password="+password1;
        String url = "http://192.168.0.108:80/api_server/login.php?mail=" + user1 + "&pwd=" + password1;


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("SRIDHAR", response.toString());
//                        Toast.makeText(Image_upload_serverActivity.this,response.toString(), Toast.LENGTH_SHORT).show();
                        try {
//                            Toast.makeText(Image_upload_serverActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                          /*  if (response.getString("log_details").equals("Failed"))
                            {
                                Toast.makeText(Onetime_loginActivity.this, response.getString("log_details"), Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String stud_id = response.getJSONObject("log_details").getString("Student_id");
                                String stud_name = response.getJSONObject("log_details").getString("Student_name");

                                System.out.println("student id: " + stud_id + " student:name: " + stud_name );

                                dbh.deleteQuery_student_table();
                                dbh.insert_student_details(stud_id, stud_name);
                                Intent i1 = new Intent(Onetime_loginActivity.this, Login_successActivity.class);
                                i1.putExtra("key", "danial_android");
                                startActivity(i1);
                            }
*/

                            if (response.getString("status").equals("invalid credentails")) {
                                Toast.makeText(Onetime_loginActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                            } else if (response.getString("status").equals("Check Password")) {
                                Toast.makeText(Onetime_loginActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                            } else {

                                String stud_id = response.getJSONArray("status").getJSONObject(0).getString("Student_id");
                                String stud_name = response.getJSONArray("status").getJSONObject(0).getString("Student_name");

                                System.out.println("student id: " + stud_id + " student:name: " + stud_name);

                                dbh.deleteQuery_student_table();
                                dbh.insert_student_details(stud_id, stud_name);
                                Intent i1 = new Intent(Onetime_loginActivity.this, Login_successActivity.class);
                                i1.putExtra("key", "Aditya_android");
                                startActivity(i1);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("SRIDHAR", "Error: " + error.getMessage());
                Toast.makeText(Onetime_loginActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                pDialog.hide();
            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }

    @Override
    protected void onPause() {
        super.onPause();
        /*if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }

        finish();*/
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (pDialog != null) {
        }
    }
}
