package learnesay.lreanfrombasic;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

public class TextFieldsActivity extends AppCompatActivity {
    LinearLayout l1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_fields);

        l1=(LinearLayout)findViewById(R.id.activity_text_fields);
        final EditText name=(EditText)findViewById(R.id.edt_name);
        final TextView text_view=(TextView)findViewById(R.id.txt1);
        Button btn_submit=(Button)findViewById(R.id.submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name_stng=name.getText().toString();
                text_view.setText(name_stng);
               //text_view.setText(name.getText().toString());
            }
        });

        try {
            InputStream ims = getAssets().open("android_image_8.jpg");
            Drawable d = Drawable.createFromStream(ims, null);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
