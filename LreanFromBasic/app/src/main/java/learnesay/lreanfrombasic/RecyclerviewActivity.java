package learnesay.lreanfrombasic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.widget.RelativeLayout;

public class RecyclerviewActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    int width,height;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);
        getSupportActionBar().hide();

        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        //layoutManager = new GridLayoutManager(this,2);
        //recyclerView.setLayoutManager(layoutManager);
       // ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) recyclerView.getLayoutParams();
       // marginLayoutParams.setMargins(50, 10, 150, 10);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);


        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams1.width = (int) (width *0.90);
        layoutParams1.height = (int) (height * 0.30);
        layoutParams1.setMargins((int) (width * 0.05), (int) (height * 0.05), (int) (width * 0), (int) (height * 0));
        recyclerView.setLayoutParams(layoutParams1);

    }
}
