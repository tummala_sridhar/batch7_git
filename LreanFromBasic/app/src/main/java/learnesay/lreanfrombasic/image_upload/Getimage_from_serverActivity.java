package learnesay.lreanfrombasic.image_upload;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;

import learnesay.lreanfrombasic.R;
import learnesay.lreanfrombasic.app.AppController;

public class Getimage_from_serverActivity extends AppCompatActivity {
    String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_image_from_server);
      //  getSupportActionBar().hide();

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            data = extras.getString("name");
        }



        //String data1= getIntent().getStringExtra("keyName");
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();

        final NetworkImageView advImage=(NetworkImageView)findViewById(R.id.networkImageView);
       // advImage.setImageUrl(getResources().getString(R.string.ipconfig)+"/uploadimage/"+data, AppController.getInstance().getImageLoader());
        advImage.setImageUrl("http://192.168.0.5:80/uploadimage/"+data, AppController.getInstance().getImageLoader());
       // advImage.setImageUrl("http://10.0.2.2/uploadimage/"+data, AppController.getInstance().getImageLoader());


    }
}
