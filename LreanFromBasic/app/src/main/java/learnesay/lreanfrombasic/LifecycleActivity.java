package learnesay.lreanfrombasic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LifecycleActivity extends Activity {

    final String TAG = "LifecycleActivity";

    Button btnActTwo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifecycle);

        btnActTwo = (Button) findViewById(R.id.btnActTwo);
        btnActTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Toast toast=Toast.makeText(getApplicationContext(),"sridhar",Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP,0,0);
                toast.show();

                *//*Custom Toast*//*
                LayoutInflater infilate=getLayoutInflater();
               // View layout=infilate.inflate(R.layout.custom_xlm,(ViewGroup)findViewById(R.id.rootlayout_id));
                Toast cust_toast=new Toast(getApplicationContext());
                cust_toast.setGravity(Gravity.TOP,0,0);
                cust_toast.setDuration(Toast.LENGTH_SHORT);
               // cust_toast.setView(layout);
                cust_toast.show();*/





                Intent i1=new Intent(LifecycleActivity.this,lifecycle2Activity.class);
                startActivity(i1);
                finish();
                finishAffinity();
            }
        });

        Log.d(TAG, "Image_upload_serverActivity: onCreate()");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Image_upload_serverActivity: onRestart()");
    }


    @Override
        protected void onStart() {
            super.onStart();
            Log.d(TAG, "Image_upload_serverActivity: onStart()");

        }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Image_upload_serverActivity: onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Image_upload_serverActivity: onPause()");
       // finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Image_upload_serverActivity: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Image_upload_serverActivity: onDestroy()");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}