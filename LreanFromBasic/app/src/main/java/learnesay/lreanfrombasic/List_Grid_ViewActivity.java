package learnesay.lreanfrombasic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

public class List_Grid_ViewActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list__grid__view);

        listView = (ListView) findViewById(R.id.list);

        // Defined Array values to show in ListView
        String[] values1 = new String[]{ "List 1", "List 2", "List 3", "List 4", "List 5", "List 6", "List 7", "List 8"};


        // storing string resources into Array
        String[] values = getResources().getStringArray(R.array.adobe_products);

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, values);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();

            }

        });


        GridView gridview = (GridView) findViewById(R.id.gridview);

        ArrayAdapter<String> adapter_grid = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values1);
        gridview.setAdapter(adapter_grid);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition     = position;
                String  itemValue    = (String) listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  GridItem : " +itemValue , Toast.LENGTH_LONG).show();
            }
        });


    }
}
