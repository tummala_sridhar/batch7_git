package learnesay.lreanfrombasic.etc;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.plus.PlusShare;

import java.io.FileNotFoundException;
import java.io.InputStream;

import learnesay.lreanfrombasic.R;
import learnesay.lreanfrombasic.app.AppController;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class ShareActivity extends AppCompatActivity {

    private static final int WRITE_EXTERNAL_STORAGE_constant = 10;

    private static final int SELECT_PHOTO = 100;
    private String TAG;
    public Bitmap Cbitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Button btn_share = (Button) findViewById(R.id.button7);
        Button btn_share_apps = (Button) findViewById(R.id.button12);
        Button btn_share_img = (Button) findViewById(R.id.button13);
        Button btn_share_img_gallery = (Button)findViewById(R.id.button14);
        Button btn_share_video = (Button)findViewById(R.id.button15);
        Button btn_share_audio = (Button)findViewById(R.id.button16);

        String imageUri = "http://basolutions.in/m4d/uploads/images/img1.jpg";
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        // If you are using normal ImageView
        imageLoader.get(imageUri, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    //imageView.setImageBitmap(response.getBitmap());
                    Cbitmap=response.getBitmap();
                }
            }
        });

        if (ContextCompat.checkSelfPermission(ShareActivity.this, WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // We do not have this permission. Let's ask the user
            ActivityCompat.requestPermissions(ShareActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_constant);
        }




        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Launch the Google+ share dialog with attribution to your app.
                Intent shareIntent = new PlusShare.Builder(ShareActivity.this)
                        .setType("text/plain")
                        .setText("Welcome to android")
                        .setContentUrl(Uri.parse("http://basolutions.in/m4d/uploads/images/img1.jpg"))
                        .getIntent();
                startActivity(Intent.createChooser(shareIntent, "Invite link!"));
                // startActivityForResult(shareIntent, 0);
            }
        });

        btn_share_apps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Your body here";
                String shareSub = "Your subject here";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });

        btn_share_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Bitmap imgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.banner1);
                String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), imgBitmap, "title", null);
                Uri imgBitmapUri = Uri.parse(imgBitmapPath);

                String text = "Look at my awesome picture";
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                shareIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri);
               // shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
                shareIntent.setType("image*//*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share images..."));*/

                

                String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), Cbitmap, "title", null);
                Uri imgBitmapUri = Uri.parse(imgBitmapPath);

                String text = "Look at my awesome picture";
                Intent shareIntent = new Intent();
                shareIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri);
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                shareIntent.setType("image/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share images..."));

            }
        });

        btn_share_img_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });

        btn_share_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT <= 19) {
                    Intent i = new Intent();
                    i.setType("video/*");
                    i.setAction(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(i, 20);
                } else if (Build.VERSION.SDK_INT > 19) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 20);
                }
            }
        });
        btn_share_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 21);
            }
        });

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case WRITE_EXTERNAL_STORAGE_constant: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted!
                    // you may now do the action that requires this permission
                } else {
                    // permission denied
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), yourSelectedImage, "title", null);
                    Uri yourSelectedImage_Uri = Uri.parse(imgBitmapPath);

                    String text = "Image From Gallery";
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, yourSelectedImage_Uri);
                    shareIntent.setType("image/*, video/*");
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(shareIntent, "Share images..."));


                }
        }


                if (requestCode == 20) {
                    Uri selectedVideoUri = imageReturnedIntent.getData();
                    String selectedVideoPath = getRealPathFromURI(selectedVideoUri);

                    if (selectedVideoPath != null) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, selectedVideoPath);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, selectedVideoUri);
                        shareIntent.setType("video/*");
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(shareIntent, "Share video..."));
                    }
                }

      else if (requestCode == 21) {
                    Uri selectedaudio_Uri = imageReturnedIntent.getData();
                    Intent shareIntent = new Intent();
                    String text = "Audio From Gallery";
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, selectedaudio_Uri);
                    shareIntent.setType("audio/*");
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(shareIntent, "Share Audio..."));
        }


    }

    public String getRealPathFromURI(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

}