package learnesay.lreanfrombasic;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Alert_progrees_ToastActivity extends AppCompatActivity {
    Button alert,alert2,alert3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_box);



         alert = (Button) findViewById(R.id.alert1);
         alert2 = (Button) findViewById(R.id.alert2);
         alert3 = (Button) findViewById(R.id.alert3);


     /*Screen Orientation starts*//*

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });


        alert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        });*/

        /*  Screen Orientation Stops */


        /*Alerts Start*/

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Alert_progrees_ToastActivity.this);
                alertDialogBuilder.setIcon(R.drawable.a1);
                alertDialogBuilder.setTitle("Alert");
                alertDialogBuilder.setMessage("Hai welcome to android Tutorails");
                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        // Write your code here to invoke YES event
                        Toast.makeText(getApplicationContext(), "You clicked on Ok", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialogBuilder.show();
            }
        });

        // First, use the AlertDialog.Builder to create the alert box interface,
        // like title, message to display, buttons, and button onclick function

      alert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Alert_progrees_ToastActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you, sure you want delete this?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.a1);

                alertDialog.setCancelable(true);

              alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        Toast.makeText(Alert_progrees_ToastActivity.this, "clicked on cancel button", Toast.LENGTH_SHORT).show();
                    }
                });
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        // Write your code here to invoke YES event
                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();


            }
        });


        alert3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Alert_progrees_ToastActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Save File...");

                // Setting Dialog Message
                alertDialog.setMessage("Do you want to save this file?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.a1);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed YES button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on YES",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed No button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Netural "Cancel" Button
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed Cancel button. Write Logic Here
                        Toast.makeText(getApplicationContext(), "You clicked on Cancel",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Showing Alert Message
                alertDialog.show();

            }
        });

         /*Alerts Ends*/


        /*Progress Dialog starts*/
/*progress spinner */

       alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(Alert_progrees_ToastActivity.this);

                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("ProgressDialog"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(true);


                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(5000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }).start();


            }
        });

        /*progress Bar horizontal */
        alert2.setOnClickListener(new View.OnClickListener() {

             ProgressDialog progressDialog = new ProgressDialog(Alert_progrees_ToastActivity.this);

            @Override
            public void onClick(View v) {

                progressDialog.setMax(100); // Progress Dialog Max Value
                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("ProgressDialog"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); // Progress Dialog Style Horizontal
              //  progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Circle
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);

                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try {
                            while (progressDialog.getProgress() <= progressDialog.getMax())
                            {
                                // for circular
                            /*Thread.sleep(2000);
                            progressDialog.dismiss();*/

                                //For Horizontal

                                Thread.sleep(200);
                                handle.sendMessage(handle.obtainMessage());
                                if (progressDialog.getProgress() == progressDialog.getMax())
                                {
                                    progressDialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            Handler handle = new Handler()
            {
                public void handleMessage(Message msg)
                {
                    super.handleMessage(msg);
                    progressDialog.incrementProgressBy(2); // Incremented By Value 2
                }
            };

        });


        /*Toass Start*/

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "This is Toast example.", Toast.LENGTH_SHORT).show();
            }
        });

        alert2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                showCustomAlert();
            }
        });







    }

    public void showCustomAlert()
    {

        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();
        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.custom_toast, null);
        Toast toast = new Toast(getApplicationContext());
        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    /*Toass Ends*/



}