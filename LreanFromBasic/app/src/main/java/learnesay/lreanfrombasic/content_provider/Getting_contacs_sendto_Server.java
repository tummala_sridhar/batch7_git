package learnesay.lreanfrombasic.content_provider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import learnesay.lreanfrombasic.R;

public class Getting_contacs_sendto_Server extends AppCompatActivity {


    private static final int PERMISSION_REQUEST_CONTACT = 10;
    Button button;
    ListView lv;
    String contactId;
    ArrayList<HashMap<String, String>> contactData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getting_contacts);

        button = (Button) findViewById(R.id.pickcontact);
        lv = (ListView) findViewById(R.id.list);
        askForContactPermission();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetContacts().execute();

            }
        });

    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(Getting_contacs_sendto_Server.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(Getting_contacs_sendto_Server.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSION_REQUEST_CONTACT);
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT:
                {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "permission are given", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "No permission for contacts", Toast.LENGTH_SHORT).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



    private class GetContacts extends AsyncTask<String, Void, String> {
        private String TAG="Async Json";
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Getting_contacs_sendto_Server.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();


        }

        @Override
        protected String doInBackground(String... params) {
            contactData = new ArrayList<HashMap<String, String>>();
            String response = "";


            ContentResolver cr = getContentResolver();
            Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            while (cursor.moveToNext()) {
                try {
                    contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    if (Integer.parseInt(hasPhone) > 0) {
                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);

                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("name", name);
                        map.put("number", phoneNumber);
                        contactData.add(map);

                        while (phones.moveToNext()) {

                        }
                        phones.close();


                    }
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            ListAdapter adapter = new SimpleAdapter(Getting_contacs_sendto_Server.this, contactData,
                    R.layout.contact_list_item, new String[]{"name", "number"},
                    new int[]{R.id.name, R.id.mobile});

            lv.setAdapter(adapter);
            Toast.makeText(Getting_contacs_sendto_Server.this, "lenght"+contactData.size(), Toast.LENGTH_SHORT).show();
            System.out.println(contactData);

            /*JSONObject jsonOBJ = new JSONObject();
            try {
                //jsonOBJ.put("userAdding", user._id);
                JSONArray list = new JSONArray(contactData);
                jsonOBJ.put("people_ids", list);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/



        }
    }

}