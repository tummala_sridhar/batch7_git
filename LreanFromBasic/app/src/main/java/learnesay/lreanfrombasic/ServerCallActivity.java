package learnesay.lreanfrombasic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import learnesay.lreanfrombasic.app.AppController;
import learnesay.lreanfrombasic.app.CustomJsonObjectRequest;

public class ServerCallActivity extends AppCompatActivity {

    private String TAG ="ServerCallActivity" ;
    ArrayList<Student> studentsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_call);
        SendData();
    }

    private void SendData() {

        String url = "http://livework.in/expert_raja/api/get_categories";

        HashMap<String, String> params = new HashMap<>();
        params.put("", "");

        Log.d(TAG, url);
        Log.d(TAG, params.toString());

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // TODO Auto-generated method stub
                        try {

                            Log.d(TAG, "RESPONSE : " + response.toString());
                            String result = response.getString("err_code");

                            if (result.equals("valid")) {
                                studentsList.clear();

                                JSONObject object = response.getJSONObject("data");
                                JSONArray array = object.getJSONArray("category");

                                for(int i = 0; i < array.length();i++){
                                    Student model = new Student();
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    model.setName(jsonObject.getString("name"));
                                    model.setAge(jsonObject.getString("num_count"));
                                    model.setImage(jsonObject.getString("image"));
                                    studentsList.add(model);
                                }

                            }

                            loadRecycleView();
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ServerCallActivity.this, "Request Time out error. Your internet connection is too slow to work", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(ServerCallActivity.this, "Connection Server error", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(ServerCallActivity.this, "Network connection error! Check your internet Setting", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(ServerCallActivity.this, "Parsing error", Toast.LENGTH_LONG).show();
                        }

                    }
                });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    private void loadRecycleView() {
        RecyclerView recycle = (RecyclerView)findViewById(R.id.recycle);
        //  LinearLayoutManager layoutManager_ver = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //  LinearLayoutManager layoutManager_hor = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        GridLayoutManager layoutManager_grid = new GridLayoutManager(this, 1);
        recycle.setLayoutManager(layoutManager_grid);
        Rv_StudentsAdp rv_studentsAdp = new Rv_StudentsAdp(this, studentsList);
        recycle.setAdapter(rv_studentsAdp);
    }
}
