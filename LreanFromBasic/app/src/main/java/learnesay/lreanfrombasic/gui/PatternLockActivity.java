package learnesay.lreanfrombasic.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.takwolf.android.lock9.Lock9View;



import learnesay.lreanfrombasic.R;

public class PatternLockActivity extends AppCompatActivity {


    Lock9View lock9View,lock9View1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_lock);

        lock9View=(Lock9View)findViewById(R.id.lock_9_view);
        lock9View1=(Lock9View)findViewById(R.id.lock_9_view1);

        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
                Toast.makeText(PatternLockActivity.this, password, Toast.LENGTH_SHORT).show();
            }

        });

        lock9View1.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
                Toast.makeText(PatternLockActivity.this, password, Toast.LENGTH_SHORT).show();
            }

        });
    }
}
