package learnesay.lreanfrombasic.gui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnesay.lreanfrombasic.R;

/**
 * Created by sridhar on 26-01-2018.
 */

public class DialogBoxFragment extends DialogFragment
{

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.dialogfragment, container, false);
        getDialog().setTitle("DialogFragment Tutorial");
        // Do something else
        return rootView;
    }
}
