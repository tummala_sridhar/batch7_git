package learnesay.lreanfrombasic.gui;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.widget.Button;
import android.widget.RelativeLayout;

import learnesay.lreanfrombasic.R;

public class Ui_1Activity extends AppCompatActivity {

int width,height;
    Button btn1,btn2,btn3,btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui_1);

        Display  display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        btn1=(Button)findViewById(R.id.button8) ;
        btn2=(Button)findViewById(R.id.button9) ;
        btn3=(Button)findViewById(R.id.button10) ;
        btn4=(Button)findViewById(R.id.button11) ;


        RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams11.width = (int) (width * 0.30);
        layoutParams11.height = (int) (height * 0.10);
        layoutParams11.setMargins((int) (width * 0.03), (int) (height * 0.10), (int) (width * 0.01), (int) (height * 0));
        btn1.setLayoutParams(layoutParams11);

        RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams12.width = (int) (width * 0.30);
        layoutParams12.height = (int) (height * 0.10);
        layoutParams12.setMargins((int) (width * 0.36), (int) (height * 0.10), (int) (width * 0.01), (int) (height * 0));
        btn2.setLayoutParams(layoutParams12);

        RelativeLayout.LayoutParams layoutParams13 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams13.width = (int) (width * 0.30);
        layoutParams13.height = (int) (height * 0.10);
        layoutParams13.setMargins((int) (width * 0.69), (int) (height * 0.10), (int) (width * 0.01), (int) (height * 0));
        btn3.setLayoutParams(layoutParams13);

        RelativeLayout.LayoutParams layoutParams14 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams14.width = (int) (width * 0.60);
        layoutParams14.height = (int) (height * 0.10);
        layoutParams14.setMargins((int) (width * 0.20), (int) (height * 0.30), (int) (width * 0.01), (int) (height * 0));
        btn4.setLayoutParams(layoutParams14);

        btn4.setTextColor(Color.parseColor("#000000"));
        btn4.setTextSize(pxToDp((int) (height * 0.03)));


    }

    public int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
