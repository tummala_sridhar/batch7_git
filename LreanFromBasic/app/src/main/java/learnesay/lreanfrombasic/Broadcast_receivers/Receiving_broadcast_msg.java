package learnesay.lreanfrombasic.Broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.widget.Toast;

/**
 * Created by Sridhar on 08-06-2017.
 */

public class Receiving_broadcast_msg  extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Intent Detected.", Toast.LENGTH_LONG).show();


        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;

        Toast.makeText(context, "Charging : "+isCharging, Toast.LENGTH_SHORT).show();

        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


        Toast.makeText(context, "usbCharge : "+usbCharge, Toast.LENGTH_SHORT).show();
        Toast.makeText(context, "acCharge : "+acCharge, Toast.LENGTH_SHORT).show();





}
}
