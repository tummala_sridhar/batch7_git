package learnesay.lreanfrombasic.Maps;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import learnesay.lreanfrombasic.R;
import learnesay.lreanfrombasic.app.AppController;

public class Map_marker_ongiven_lat_longActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private static LatLng lat_long = new LatLng(17.6868159, 83.2184815);
    private Marker marker;
    private GoogleMap mMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_marker_ongiven_lat_long);
      // json_address();
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add some markers to the map, and add a data object to each marker.
        marker = mMap.addMarker(new MarkerOptions()
                .position(lat_long)
                .title("Vizag"));
        marker.setTag(0);


        mMap.setOnMarkerClickListener(this);

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(marker.getPosition());

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded()
            {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(4));
            }
        });


    }


    public void json_address() {

        String url = "http://mygeniusapp.com/app/admin/merchant-contact-api.php?id=12";
        //  String url = "http://mygeniusapp.com/app/admin/api/listing-categories2.php";
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Tab2", response.toString());

                        try {

                            if(response.getJSONArray("Merchant_Data").getJSONObject(0).getString("id").equals("NoRecords"))
                            {
                                Toast.makeText(Map_marker_ongiven_lat_longActivity.this, "NO RECORDS", Toast.LENGTH_SHORT).show();

                            }
                            else {
                                int size = response.getJSONArray("Merchant_Data").length();
                                System.out.println("length: " + size);
                                // Toast.makeText(getActivity(), ""+response.getJSONArray("Restaurant").toString(), Toast.LENGTH_SHORT).show();

                                String  list_address=response.getJSONArray("Merchant_Data").getJSONObject(0).getString("Address");
                                String  list_latitude1=response.getJSONArray("Merchant_Data").getJSONObject(0).getString("lat");
                                String list_longitude1=response.getJSONArray("Merchant_Data").getJSONObject(0).getString("lon");


                                System.out.println("Address: : " + list_address);
                                System.out.println("Latitude: : " + list_latitude1);
                                System.out.println("Longitude: : " + list_longitude1);


                              Double  lat=Double.parseDouble(list_latitude1);
                              Double  lon=Double.parseDouble(list_longitude1);

                                System.out.println("Longitude: : " + lat +"\n Longitude: : " + lon);

                                lat_long=new LatLng(lat,lon);
                                // address.setText(list_address);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Map_marker_ongiven_lat_longActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
                        }
                        // pDialog.hide();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("genius", "Error: " + error.getMessage());
                Toast.makeText(Map_marker_ongiven_lat_longActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("Json Error: "+error.getMessage());
                // hide the progress dialog
                // pDialog.hide();


            }
        });


        // Adding request to request queue
        String tag_json_obj="json";
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        mMap.moveCamera(CameraUpdateFactory.newLatLng(lat_long));
        float zoomLevel =(float)16.0; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_long, zoomLevel));



        return false;
    }
}
