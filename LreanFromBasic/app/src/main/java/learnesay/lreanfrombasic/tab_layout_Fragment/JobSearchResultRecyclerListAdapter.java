package learnesay.lreanfrombasic.tab_layout_Fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import learnesay.lreanfrombasic.R;


/**
 * Created by sridhar on 24-02-2018.
 */

public class JobSearchResultRecyclerListAdapter extends RecyclerView.Adapter<JobSearchResultRecyclerListAdapter.ViewHolder> implements View.OnClickListener {

    Context mcontext;
    private int lastPosition = -1;
    private int animtype;

    public JobSearchResultRecyclerListAdapter(Context mcontext, int animtype) {
        this.mcontext = mcontext;
        this.animtype = animtype;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       // View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.jobsearchresult_list_item, null);
         View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.realestate_list_item, null);
         ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mcontext, animtype);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public void onClick(View view)
    {
        int position = (Integer) view.getTag();
        Toast.makeText(mcontext, String.valueOf(position), Toast.LENGTH_SHORT).show();

    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
