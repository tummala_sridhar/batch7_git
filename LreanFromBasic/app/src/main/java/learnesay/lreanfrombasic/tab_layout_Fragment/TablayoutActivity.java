package learnesay.lreanfrombasic.tab_layout_Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import learnesay.lreanfrombasic.R;

public class TablayoutActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private TabLayout tabLayout;
    ViewPager viewPager;
    private ArrayList<String> tab_title;
    int tab_position = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablayout);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);


        tab_title = new ArrayList<String>();
        tab_title.add("Title11");
        tab_title.add("Title22");
        tab_title.add("Title33");

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title3"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title4"));

        /*for(int i=0;i<tab_title.size();i++)
        {
            tabLayout.addTab(tabLayout.newTab().setText(tab_title.get(i)));
        }*/

        //  tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //on swipe changing the tab indicator position
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //Creating our pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        // tabLayout.setOnTabSelectedListener(this);
        tabLayout.addOnTabSelectedListener(this);
        try {
            TabLayout.Tab tab = tabLayout.getTabAt(1);
            tab.select();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        //  tab_position=tab.getPosition();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
