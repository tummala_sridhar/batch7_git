package learnesay.lreanfrombasic.tab_layout_Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import learnesay.lreanfrombasic.R;

/**
 * Created by Sridhar on 02-06-2017.
 */

public class Tab2 extends Fragment {
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.tab2, container, false);

        TextView text1=(TextView)view.findViewById(R.id.textView);
        text1.setText("Sridhar");


         return view;

    }


}
