package learnesay.lreanfrombasic.tab_layout_Fragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Sridhar on 02-06-2017.
 */

public class Pager extends FragmentStatePagerAdapter {
    //integer to count number of tabs
   private int tabCount;

    //Constructor to the class
    Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Tab1();
            case 1:
                return new Batch5Fragment();
            case 2:
                Tab3 tab3 = new Tab3();
                return tab3;
            case 3:
                Tab4Fragment tab4 = new Tab4Fragment();
                return tab4;

            default:

                return null;
        }
    }


    @Override
    public int getCount()
    {
        return tabCount;
    }

}