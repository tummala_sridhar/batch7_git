package learnesay.lreanfrombasic;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

public class WidegtsActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    TextView txt_seek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widegts);

        /*Toggle Button Stats*/

        final ToggleButton tg_btn1 = (ToggleButton) findViewById(R.id.toggleButton2);
        final TextView txt1 = (TextView) findViewById(R.id.textView);

        tg_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt1.setText(tg_btn1.getText());
            }
        });


        /*Toggle Button Ends*/

        /*Radio Group Starts*/
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rg1);

        final TextView tex_rg = (TextView) findViewById(R.id.tex_rg);
        final RadioButton b = (RadioButton) findViewById(R.id.r1);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.r1) {
                    Toast.makeText(getApplicationContext(), "R1 ", Toast.LENGTH_SHORT).show();
                    tex_rg.setText("R1");
                } else if (checkedId == R.id.r2) {
                    Toast.makeText(getApplicationContext(), "R2", Toast.LENGTH_SHORT).show();
                    tex_rg.setText("R2");
                } else if (checkedId == R.id.r3) {
                    Toast.makeText(getApplicationContext(), "R3", Toast.LENGTH_SHORT).show();
                    tex_rg.setText("R3");
                } else if (checkedId == R.id.r4) {
                    Toast.makeText(getApplicationContext(), "R4", Toast.LENGTH_SHORT).show();
                    tex_rg.setText("R4");
                }
            }
        });
        /*Radio Group Ends*/


        /*checkBox  Starts*/

        final CheckBox ch1 = (CheckBox) findViewById(R.id.checkBox1);
        final CheckBox ch2 = (CheckBox) findViewById(R.id.checkBox2);
        final TextView txt_check = (TextView) findViewById(R.id.txt_check);
        // ch1.setChecked(true);
        ch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                txt_check.setText("" + isChecked);

               /* if(isChecked){
                    ch2.setChecked(true);
                }*/

                if (buttonView.isPressed()) {
                    if (ch1.isChecked()) {
                        txt_check.setText("checked");
                    } else {
                        txt_check.setText("unchecked");
                    }

                    Toast.makeText(WidegtsActivity.this, "Check BOX ONE", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //  txt_check.setText("" + isChecked);

                StringBuffer result = new StringBuffer();

                result.append(" ch1  ").append(ch1.isChecked());
                result.append(" and  ch2  ").append(ch2.isChecked());

                txt_check.setText(result);
            }
        });


        Button bnt_check = (Button) findViewById(R.id.btn_check);

        bnt_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer result = new StringBuffer();
                result.append(" ch1  ").append(ch1.isChecked());
                result.append(" and  ch2  ").append(ch2.isChecked());

                txt_check.setText(result);
            }
        });


        /*checkBox  Ends*/


        /* Spinner Starts*/

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final TextView txt_spn = (TextView) findViewById(R.id.spin1);
        final Button bnt_spn = (Button) findViewById(R.id.button);


        // Spinner Drop down elements
        final ArrayList<String> categories = new ArrayList<String>();
        categories.add("Select");
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");

// Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), adapterView.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                // txt_spn.setText(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bnt_spn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String teacher_id = categories.get(spinner.getSelectedItemPosition());
                String teacher_id2 = spinner.getSelectedItem().toString();

                if (teacher_id.equals("Select")) {
                    Toast.makeText(getApplicationContext(), "Please select other", Toast.LENGTH_SHORT).show();
                } else {
                    txt_spn.setText(teacher_id);
                    Toast.makeText(getApplicationContext(), teacher_id2, Toast.LENGTH_SHORT).show();
                }
            }
        });


        /* Spinner Ends*/



        /*Progress BAR starts*/

       /* final Button btn_progress1=(Button)findViewById(R.id.btn_progress1);
        final Button btn_progress2=(Button)findViewById(R.id.btn_progress2);
        final ProgressBar btn_prgbr=(ProgressBar)findViewById(R.id.progressBar1);
*/

        /*Progress BAR Ends*/


        /*Seekbar Starts*/

        SeekBar seek_bar = (SeekBar) findViewById(R.id.sek_seekBar);
        txt_seek = (TextView) findViewById(R.id.txt_seekbar1);
        int maxValue = seek_bar.getMax();  // get maximum value of the Seek bar
        int seekBarValue = seek_bar.getProgress(); // get progress value from the Seek bar
        Toast.makeText(this, "max value : " + maxValue + " Seek Bar Value :" + seekBarValue, Toast.LENGTH_SHORT).show();

        seek_bar.setOnSeekBarChangeListener(this);
        /*Seekbar Ends*/


    }


    /*Seekbar methods Start*/
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // Toast.makeText(getApplicationContext(),"seekbar progress: "+progress, Toast.LENGTH_SHORT).show();
        txt_seek.setText("Progress " + progress);
        if (progress > 98) {
            Toast.makeText(this, "greater 50", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Toast.makeText(getApplicationContext(), "seekbar touch started!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Toast.makeText(getApplicationContext(), "seekbar touch stopped!", Toast.LENGTH_SHORT).show();
    }



    /*Seekbar methods ends*/
}
