package learnesay.lreanfrombasic.RND;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.widget.Toast;

import learnesay.lreanfrombasic.R;

public class multi_device_screen_resoluActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_device_screen_resolu);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            Toast.makeText(getApplicationContext(), "PORTRAIT_sri",
                    Toast.LENGTH_SHORT).show();
        } else {
            //code for landscape mode
            Toast.makeText(getApplicationContext(), "LandScape_sri",
                    Toast.LENGTH_SHORT).show();
        }


        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Toast.makeText(getApplicationContext(), "" + width + "," + height,
                Toast.LENGTH_SHORT).show();
        if (width > height) {
            Toast.makeText(getApplicationContext(), "LandScape",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "PORTRIAT",
                    Toast.LENGTH_SHORT).show();
        }

    }


}
