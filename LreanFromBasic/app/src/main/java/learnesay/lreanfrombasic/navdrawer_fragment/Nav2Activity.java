package learnesay.lreanfrombasic.navdrawer_fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import learnesay.lreanfrombasic.CircularNetworkImageView;
import learnesay.lreanfrombasic.R;

public class Nav2Activity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView navbutton;
    int width, height;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav2);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();

        TextView title = (TextView) toolbar.findViewById(R.id.title);

        CircularNetworkImageView img_icon=(CircularNetworkImageView)findViewById(R.id.logo_id);
        RelativeLayout.LayoutParams layoutParams21 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams21.width = (int) (width * 0.16);
        layoutParams21.height = (int) (height * 0.15);
        layoutParams21.setMargins((int) (width * 0.01), (int) (height * 0.01), (int) (width * 0), (int) (height * 0));
        img_icon.setLayoutParams(layoutParams21);

        TextView name1=(TextView)findViewById(R.id.name);
        RelativeLayout.LayoutParams layoutParams22 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams22.width = (int) (width * 0.70);
        layoutParams22.height = (int) (height * 0.05);
        layoutParams22.setMargins((int) (width * 0.20), (int) (height * 0.02), (int) (width * 0), (int) (height * 0));
        name1.setLayoutParams(layoutParams22);
        name1.setTextSize(pxToDp((int) (height * 0.025)));

        TextView phnumber=(TextView)findViewById(R.id.phnumber);
        RelativeLayout.LayoutParams layoutParams23 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams23.width = (int) (width * 0.70);
        layoutParams23.height = (int) (height * 0.05);
        layoutParams23.setMargins((int) (width * 0.20), (int) (height * 0.06), (int) (width * 0), (int) (height * 0));
        phnumber.setLayoutParams(layoutParams23);
        phnumber.setTextSize(pxToDp((int) (height * 0.02)));


        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams2.width = (int) (width * 0.16);
        layoutParams2.height = (int) (height * 0.11);
        layoutParams2.setMargins((int) (width * -0.05), (int) (height * 0.01), (int) (width * 0), (int) (height * 0));
        navbutton.setLayoutParams(layoutParams2);


        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams3.width = (int) (width * 0.40);
        layoutParams3.height = (int) (height * 0.10);
        layoutParams3.setMargins((int) (width * 0.27), (int) (height * 0.01), (int) (width * 0), (int) (height * 0));
        title.setLayoutParams(layoutParams3);
        title.setText(" SUB MENU");
        title.setTextSize(pxToDp((int) (height * 0.03)));

    }

    public int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    private void init() {

        navbutton = (ImageView) toolbar.findViewById(R.id.navbutton);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // initNavigationDrawer();
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        TextView mycash=(TextView)findViewById(R.id.mycash);
        mycash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView current_order=(TextView)findViewById(R.id.current_order);
        current_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView home=(TextView)findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

       /* TextView offers=(TextView)findViewById(R.id.offers);
        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,OffersActivity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView notifications=(TextView)findViewById(R.id.notifications);
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,NotificationsActivity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });*/

        TextView previousorder=(TextView)findViewById(R.id.previousorder);
        previousorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView call=(TextView)findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView about=(TextView)findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(intent);
            }
        });

        TextView logout=(TextView)findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent12=new Intent(Nav2Activity.this,Nav2Activity.class);
                drawerLayout.closeDrawer(GravityCompat.START);

                startActivity(intent12);
                finish();
            }
        });


    }
}