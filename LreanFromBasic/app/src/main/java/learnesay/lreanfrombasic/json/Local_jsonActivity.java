package learnesay.lreanfrombasic.json;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import learnesay.lreanfrombasic.R;

public class Local_jsonActivity extends AppCompatActivity {
    ListView list1;
    ArrayList<String> name,email,mobile;
    private int length;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_json);

        list1= (ListView)findViewById(R.id.list);

        name = new ArrayList<String>();
        email = new ArrayList<String>();
        mobile = new ArrayList<String>();


        StringBuilder buf=new StringBuilder();
        InputStream json= null;
        String str;
        BufferedReader in= null;

/*reading json data from assests folder*/

        try {
            json = getAssets().open("jsondata.json");
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

            while ((str=in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

/*Extracting  json data responce*/

        try {
            JSONObject jsonObj = new JSONObject(String.valueOf(buf));

            for (int i = 0; i < jsonObj.getJSONArray("contacts").length(); i++)
            {

                name.add(jsonObj.getJSONArray("contacts").getJSONObject(i).getString("name").trim());
                email.add(jsonObj.getJSONArray("contacts").getJSONObject(i).getString("email").trim());
               // mobile.add(jsonObj.getJSONArray("contacts").getJSONObject(i).getString("mobile").trim());
            }

            if(name.size()>0)
            {
                list1.setAdapter(new listadapter());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private class listadapter implements ListAdapter {
        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount()
        {
            return name.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) Local_jsonActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inf.inflate(R.layout.json_async_list_item, null);
            }
            TextView txt_name=(TextView)convertView.findViewById(R.id.name);
            TextView txt_email=(TextView)convertView.findViewById(R.id.email);


            txt_name.setText(name.get(position));
            txt_email.setText(email.get(position));


            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return name.size();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}
