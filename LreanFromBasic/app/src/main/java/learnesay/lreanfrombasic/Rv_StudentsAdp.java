package learnesay.lreanfrombasic;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import learnesay.lreanfrombasic.app.AppController;

public class Rv_StudentsAdp extends RecyclerView.Adapter<Rv_StudentsAdp.ViewHolder> {

    private Context mcontext;
    private ArrayList<Student> studentsList;

    public Rv_StudentsAdp(Context mcontext, ArrayList<Student> studentsList) {
        this.mcontext = mcontext;
        this.studentsList = studentsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_student, viewGroup, false);

        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        Student model = studentsList.get(position);

        //viewHolder.img_student.setImageDrawable(ContextCompat.getDrawable(mcontext, model.getImage()));
        viewHolder.img_student.setImageUrl(model.getImage(), AppController.getInstance().getImageLoader());
        viewHolder.txt_name.setText(model.getName());
        viewHolder.txt_age.setText(String.valueOf(model.getAge()));
        // viewHolder.txt_age.setText(""+model.getAge());

    }

    @Override
    public int getItemCount() {
        return studentsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        NetworkImageView img_student;
        TextView txt_name, txt_age, txt_desc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_student = (NetworkImageView) itemView.findViewById(R.id.img_student);
            txt_name = (TextView) itemView.findViewById(R.id.txt_name);
            txt_age = (TextView) itemView.findViewById(R.id.txt_age);

        }
    }
}
