package learnesay.lreanfrombasic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class slider_cardview_recyclerActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    LinearLayoutManager HorizontalLayoutmanager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_cardview_recycler);
        getSupportActionBar().hide();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        HorizontalLayoutmanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(HorizontalLayoutmanager);
        HorizontalAdapter HorizontalAdapter = new HorizontalAdapter();
        recyclerView.setAdapter(HorizontalAdapter);
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.ViewHolder>
    {
        ArrayList<Integer> list_images_h;
        int width,height;


        public  HorizontalAdapter( )
        {
            list_images_h=new ArrayList<Integer>();
            list_images_h.add(R.drawable.slide1);
            list_images_h.add(R.drawable.slide2);
            list_images_h.add(R.drawable.slide3);
            //list_images_h.add(R.drawable.a4);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listcardview, parent, false);
            ViewHolder viewHolder = new ViewHolder(layoutView);
            Display display = slider_cardview_recyclerActivity.this.getWindowManager().getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();
            return viewHolder;

        }


        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.img_card.setImageResource(list_images_h.get(position));

        }

        @Override
        public int getItemCount() {
            return list_images_h.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_card;
            public ViewHolder(View itemView) {
                super(itemView);
                //itemView.setLayoutParams(new LinearLayout.LayoutParams((int) (width * 0.90), (int) (width * 0.90)));
                img_card = (ImageView) itemView.findViewById(R.id.img_card);



            }
        }

    }


}
