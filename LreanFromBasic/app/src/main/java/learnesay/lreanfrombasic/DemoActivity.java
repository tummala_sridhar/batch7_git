package learnesay.lreanfrombasic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static learnesay.lreanfrombasic.R.id.num1;
import static learnesay.lreanfrombasic.R.id.num2;


public class DemoActivity extends AppCompatActivity {

    String a=null,b=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);


        final EditText enum1=(EditText)findViewById(num1);
        final EditText enum2=(EditText)findViewById(num2);
        final TextView result1=(TextView)findViewById(R.id.result);




        Button add=(Button)findViewById(R.id.add);
        Button sub=(Button)findViewById(R.id.sub);
        Button mul=(Button)findViewById(R.id.mul);
        Button div=(Button)findViewById(R.id.div);




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    //int i=Integer.parseInt(enum1.getText().toString().trim())+Integer.parseInt(enum2.getText().toString().trim());
                int num1,num2;
                try {


                    a = enum1.getText().toString().trim();
                    b = enum2.getText().toString().trim();

                    Log.d("DemoActivity", "onClick: "+a +"  "+b);

                     num1 = Integer.parseInt(a);
                     num2 = Integer.parseInt(b);
                    if(a.equals(null))
                    {
                         num1 = 0;
                    }

                    if(b.equals(null))
                    {
                         num2 = 0;
                    }

                    int i = num1 + num2;
                    result1.setText("" + i);
                }
                catch (NumberFormatException ex)
                {
                    result1.setText("0");
                }

            }
        });



        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    //int i=Integer.parseInt(enum1.getText().toString().trim())-Integer.parseInt(enum2.getText().toString().trim());
                try {
                    final int num1 = Integer.parseInt(enum1.getText().toString().trim());
                    final int num2 = Integer.parseInt(enum2.getText().toString().trim());
                    int i = num1 - num2;
                    result1.setText("" + i);
                }
                catch (NumberFormatException ex)
                {
                    result1.setText("0");
                }

            }
        });



        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                  int i=Integer.parseInt(enum1.getText().toString().trim())*Integer.parseInt(enum2.getText().toString().trim());
                    result1.setText(""+i);
                }catch (NumberFormatException e){
                    result1.setText("0");
                }

            }
        });



        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    double i=Double.parseDouble(enum1.getText().toString().trim())/Double.parseDouble(enum2.getText().toString().trim());
                    result1.setText(""+i);
                }
                catch (NumberFormatException e){
                    result1.setText("0");
                }

            }
        });

    }

}
