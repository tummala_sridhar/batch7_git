package learnesay.lreanfrombasic.multimedia;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;

import learnesay.lreanfrombasic.R;

public class MediaActivity extends AppCompatActivity {

    int count=0 ;
    Switch btn1;
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        btn1=(Switch)findViewById(R.id.button6);
        mediaPlayer = MediaPlayer.create(MediaActivity.this, R.raw.sampleaudio);

        btn1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    mediaPlayer.start(); // no need to call prepare(); create() does that for you
                    btn1.setText("Stop");
                }
                else
                {
                    mediaPlayer.stop(); // no need to call prepare(); create() does that for you
                    btn1.setText("Start");
                }
            }
        });




    }
}
