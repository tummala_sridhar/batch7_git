package learnesay.lreanfrombasic.storage_data;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import learnesay.lreanfrombasic.R;

public class Login_successActivity extends AppCompatActivity {
    DBHelper dbh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);
        dbh = new DBHelper(this);

        String fName = getIntent().getStringExtra("key");

        String id=dbh.get_student_id();
        String name=dbh.get_student_name();

        TextView txt_id=(TextView)findViewById(R.id.txt_id);
        TextView txt_name=(TextView)findViewById(R.id.txt_name);

        txt_id.setText(id +" "+fName);
        txt_name.setText(name);
    }
}
