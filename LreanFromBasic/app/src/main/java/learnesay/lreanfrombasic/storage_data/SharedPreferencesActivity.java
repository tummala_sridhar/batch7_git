package learnesay.lreanfrombasic.storage_data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import learnesay.lreanfrombasic.R;

public class SharedPreferencesActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    EditText edt_name, edt_email, edt_surname;
    Button btn_submit, btn_clear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        sharedPref = getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();


        edt_name = (EditText) findViewById(R.id.editText8);
        edt_email = (EditText) findViewById(R.id.editText9);
        edt_surname = (EditText) findViewById(R.id.editText10);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_submit.setOnClickListener(this);
        btn_clear.setOnClickListener(this);


        String name = sharedPref.getString("key_name", null); // getting String
        String email = sharedPref.getString("key_email", null);
        String surname = sharedPref.getString("key_surname", null);

         /* sharedPref.getInt("key_name", null); // getting Integer
        sharedPref.getFloat("key_name", null); // getting Float
        sharedPref.getLong("key_name", null); // getting Long
        sharedPref.getBoolean("key_name", null); // getting boolean*/

        edt_name.setText(name);
        edt_email.setText(email);
        edt_surname.setText(surname);
    }

    @Override
    public void onClick(View v) {

        if (v == btn_submit) {
            submit();
        }

        if (v == btn_clear) {
            editor.clear();
            editor.commit();
            Intent i1 = new Intent(SharedPreferencesActivity.this, SharedPreferencesActivity.class);
            startActivity(i1);
            Toast.makeText(this, "Data cleared", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void submit() {

        editor.putBoolean("key_boolean", true); // Storing boolean - true/false
        editor.putString("key_name", edt_name.getText().toString()); // Storing string
        editor.putString("key_email", edt_email.getText().toString()); // Storing string
        editor.putString("key_surname", edt_surname.getText().toString()); // Storing string
        editor.putInt("key_num", 1024); // Storing integer
        // editor.putFloat("key_name", "float value"); // Storing float
        // editor.putLong("key_name", "long value"); // Storing long

        editor.commit(); // commit changes
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();

       /* Intent i1=new Intent(SharedPreferencesActivity.this,Shared_outputActivity.class);
        startActivity(i1);*/

    }


}
