package learnesay.lreanfrombasic.storage_data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS student_details (sno INTEGER PRIMARY KEY AUTOINCREMENT,student_id TEXT,student_name TEXT)");
        Log.v("db", "Created DB");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS student_details");
        onCreate(db);
    }

    public void insert_student_details(String student_id,String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("student_id",student_id);
        values.put("student_name",name);
        db.insert("student_details", null, values);
        System.out.println("From DB inserted values "+student_id+","+name);
        db.close();
    }

    public void update_username(String username,String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE student_details SET student_name='username',student_id='id' WHERE phone='phnum' ");
        System.out.println(username+","+id);
        db.close();
    }



    public String get_student_id(){
        String name = "";
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor result=dbh.rawQuery("SELECT * FROM student_details",null);
        if(result.moveToFirst()){
            do{
                name=result.getString(result.getColumnIndex("student_id"));
            }while (result.moveToNext());
        }
        return name;
    }




    public String get_student_name(){
        String name = "";
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor result=dbh.rawQuery("SELECT * FROM student_details",null);
        if(result.moveToFirst()){
            do{
                name=result.getString(result.getColumnIndex("student_name"));
            }while (result.moveToNext());
        }
        return name;
    }


    public void deleteQuery_student_table(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM student_details"); //delete all rows in a table
        db.close();
    }

    public void deleteQuery_particular_user(String ph){
        String phone= ph;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM orders_table where user_id="+phone); //delete all rows in a table
        db.close();
    }


    public int row_count()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int numRows = (int) DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM student_details", null);
        return numRows;
    }



}
