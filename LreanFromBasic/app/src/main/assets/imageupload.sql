-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 06:08 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imageupload`
--

-- --------------------------------------------------------

--
-- Table structure for table `mail_validation`
--

CREATE TABLE `mail_validation` (
  `id` int(11) NOT NULL,
  `email` varchar(90) NOT NULL,
  `password` varchar(90) NOT NULL,
  `Name` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mail_validation`
--

INSERT INTO `mail_validation` (`id`, `email`, `password`, `Name`) VALUES
(1024, 'sridhar', '123456', 'Sridhar_ANDROID');

-- --------------------------------------------------------

--
-- Table structure for table `volleyupload`
--

CREATE TABLE `volleyupload` (
  `id` int(11) NOT NULL,
  `photo` varchar(1200) NOT NULL,
  `name` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `volleyupload`
--

INSERT INTO `volleyupload` (`id`, `photo`, `name`) VALUES
(1, 'localhost:80/uploadimage/uploads/0.png', 'Lap'),
(2, 'localhost:80/uploadimage/uploads/1.png', ''),
(3, 'localhost:80/uploadimage/uploads/2.png', 'Mouse'),
(4, 'localhost:80/uploadimage/uploads/3.png', ''),
(5, 'localhost:80/uploadimage/uploads/4.png', ''),
(6, 'localhost:80/uploadimage/uploads/5.png', 'Table'),
(7, 'localhost:80/uploadimage/uploads/6.png', 'Hga'),
(8, 'localhost:80/uploadimage/uploads/Hai.7.png', 'Hai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `volleyupload`
--
ALTER TABLE `volleyupload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `volleyupload`
--
ALTER TABLE `volleyupload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
