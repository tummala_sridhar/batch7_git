package com.thecolourmoon.firebasepushnotification.sslCertificateValidation;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thecolourmoon.firebasepushnotification.R;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class SslCertificateValidationActivity extends AppCompatActivity {

    String TAG = "SslCertificateValidationActivity";
    //https://developer.android.com/topic/security/best-practices

    Button btn_connect;
    TextView txt_url,tx_ssl,tx_res;
    public String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ssl_certificate_validation);
        btn_connect = findViewById(R.id.btn_connect);
        txt_url = findViewById(R.id.tx_url);
        tx_ssl = findViewById(R.id.tx_ssl);
        tx_res = findViewById(R.id.tx_res);
        tx_ssl.setVisibility(View.GONE);
        tx_res.setVisibility(View.GONE);

        url = "https://developer.android.com/training/articles/security-ssl";
        txt_url.setText(url);

        final Connection ssl = new Connection();
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ssl.execute();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class Connection extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            int output = 0;
            try {
                output = check(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(Object result) {
            Log.d(TAG, "onPostExecute: " + result);
            if (result.equals(200)) {
               // Toast.makeText(SslCertificateValidationActivity.this, "SSl Verified", Toast.LENGTH_SHORT).show();
                tx_ssl.setText("SSl Verified");
                tx_ssl.setVisibility(View.VISIBLE);
                tx_res.setVisibility(View.VISIBLE);
            } else {
               // Toast.makeText(SslCertificateValidationActivity.this, "SSl Not Verified", Toast.LENGTH_SHORT).show();
                tx_ssl.setText("SSl Not Verified");
                tx_ssl.setVisibility(View.VISIBLE);
            }
        }
    }

    private int check(String url_str) throws IOException {
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(url_str);
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new TrustManager[]{new X509TrustManager() {
                private X509Certificate[] accepted;
                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
                    }

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                    accepted = x509Certificates;
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return accepted;
                }
            }}, null);

            connection = (HttpsURLConnection) url.openConnection();
            connection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });



            connection.setSSLSocketFactory(context.getSocketFactory());
            System.out.println(connection.getResponseMessage());
            System.out.println(connection.getResponseCode());


            if (connection.getResponseCode() == 200) {
                InputStream in = connection.getInputStream();
                System.out.println(in.read());
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuffer out = new StringBuffer();
                while ((line = reader.readLine()) != null) {
                    out.append(line);
                }
                Log.d(TAG, "check: "+out.toString());
                tx_res.setText(Html.fromHtml(out.toString()));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection.getResponseCode();
    }

}
