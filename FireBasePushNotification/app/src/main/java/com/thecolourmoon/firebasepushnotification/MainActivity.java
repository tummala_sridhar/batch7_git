package com.thecolourmoon.firebasepushnotification;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity {
    Button btn_send;
    String refreshedToken;
    String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        getDataFromServer();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed_token: " + refreshedToken);
        deviceId = refreshedToken;
        /*forced crash firebase start*/
        /*Button crashButton = new Button(this);
        crashButton.setText("Crash!");
        crashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Crashlytics.getInstance().crash(); // Force a crash
            }
        });
        addContentView(crashButton, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
       */
        /*forced crash firebase Ends*/

        btn_send = findViewById(R.id.btn_send);
        btn_send.setVisibility(View.GONE);
        final SendNotification notify = new SendNotification();
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notify.execute();
            }
        });

    }

    //Send notifications using java code


    private class SendNotification extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btn_send.setVisibility(View.GONE);

        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                pushFCMNotification(deviceId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            btn_send.setVisibility(View.VISIBLE);
        }
    }

    // Method to send Notifications from server to client end.
    public final static String AUTH_KEY_FCM = "AAAAuVT7qys:APA91bGcJh8WW_cukc7KL8eJFAjPgTeO8dIF8wQuTwQ2VbBu6bOH8QR3KUxbReVRtiFkPuqv2aQdrjureS4Lpkw3SbSY7Kl88b2CM7zUjtZrVX1iBmOhZFIdyAdPI4QgDek2auKIVMCX";
    //  public final static String AUTH_KEY_FCM = "AIzaSyCMjPDA5u-BwMMVKWugQWpGUePyAJnFNLM";
    public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public void pushFCMNotification(String DeviceIdKey) throws Exception {

        String authKey = AUTH_KEY_FCM; // You FCM AUTH key
        String FMCurl = API_URL_FCM;

        URL url = new URL(FMCurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key=" + authKey);
        conn.setRequestProperty("Content-Type", "application/json");

        JSONObject data = new JSONObject();
        data.put("to", DeviceIdKey.trim());
        JSONObject info = new JSONObject();
        info.put("title", "Notification From Android"); // Notification title
        info.put("body", "Hello First Test notification"); // Notification body
        data.put("data", info);

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data.toString());
        wr.flush();
        wr.close();

        int responseCode = conn.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

    }

    @SuppressWarnings("static-access")
    public void main(String[] args) throws Exception {
        pushFCMNotification(deviceId);
    }


    ArrayList<NotificationsModel> notificationsList = new ArrayList<>();

    private void getDataFromServer() {

        notificationsList.clear();
        for (int i = 0; i < 5; i++) {
            NotificationsModel model = new NotificationsModel();
            model.setId("ID" + i);
            model.setDate("03-07-2018");
            model.setMessage("Daily Interest");
            notificationsList.add(model);
        }
        LoadView();

    }

    private void LoadView() {
        ListView notificationsListView = findViewById(R.id.lv_list);
        MyCustomlistAdapter adapter = new MyCustomlistAdapter(this, notificationsList);
        notificationsListView.setAdapter(adapter);
    }

    private class NotificationsModel {
        private String id;
        private String date;
        private String message;

        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDate() {
            return date;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private class MyCustomlistAdapter extends BaseAdapter {
        ArrayList<NotificationsModel> notificationsList;
        Context context;

        MyCustomlistAdapter(Context context, ArrayList<NotificationsModel> notificationsList) {
            this.context = context;
            this.notificationsList = notificationsList;
        }


        @Override
        public int getCount() {
            return notificationsList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.list_item_notifications, null);
            }

            NotificationsModel model = notificationsList.get(i);

            TextView txt_desc = convertView.findViewById(R.id.txt_desc);
            TextView txt_date = convertView.findViewById(R.id.txt_date);

            txt_desc.setText(model.getMessage());
            txt_date.setText(model.getDate());

            convertView.setTag(i);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int tag = (int) view.getTag();
                    if (tag == 1) {
                        String str = null;
                        //Log.d(TAG, "onClick: "+str.length());
                    } else if (tag == 2) {
                        Intent i1 = new Intent(MainActivity.this, FirebaseCrashlisticActivity.class);
                        // startActivity(i1);
                    }
                }
            });


            return convertView;
        }
    }

}
